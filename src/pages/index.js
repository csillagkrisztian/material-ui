import React from "react"
import Layout from "../components/layout"
import { Button } from "@material-ui/core"

export default function IndexPage() {
  return (
    <Layout>
      <h1>Hello!</h1>
      <Button></Button>
    </Layout>
  )
}
